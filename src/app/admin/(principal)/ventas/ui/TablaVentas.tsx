'use client'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Servicios } from '@/services';
import { Constantes } from '@/config/Constantes';
import { useEffect, useState } from 'react';
import dayjs, { Dayjs } from 'dayjs';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

interface ventaData{
  id: string
  nombre: string,
  primerApellido: string,
  segundoApellido: string,
  tipoDocumento: string,
  nroDocumento: string,
  fechaDeCompra: string,
  totalPagado: string
}

export default function TablaVentas() {

  const [lista, setLista] = useState<ventaData[]>([]);

  const listaVentas = async () => {
    const respuesta = await Servicios.get({
      url: `${Constantes.baseUrl}/ventas`,
    })
    let resp: ventaData[] = [];
    respuesta.map((venta: { [x: string]: any; }) => {
      resp.push(
        {
          id: venta['id'],
          nombre: venta['nombres'],
          primerApellido: venta['primer_apellido'],
          segundoApellido: venta['segundo_apellido'],
          tipoDocumento: venta['tipo_documento'],
          nroDocumento: venta['nro_documento'],
          fechaDeCompra: dayjs().format('YYYY-MM-DD'),
          totalPagado: venta['total_pagado']
        }
      )
    });
    setLista(resp)
    console.log(respuesta)
  }

  useEffect(()=>{
    listaVentas()
  },[])
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
          <StyledTableCell>Código de Venta</StyledTableCell>
            <StyledTableCell>Nombres</StyledTableCell>
            <StyledTableCell align="right">Primer Apellido</StyledTableCell>
            <StyledTableCell align="right">Segundo Apellido</StyledTableCell>
            <StyledTableCell align="right">Tipo de Documento</StyledTableCell>
            <StyledTableCell align="right">Nro de Documento</StyledTableCell>
            <StyledTableCell align="right">Fecha de Compra</StyledTableCell>
            <StyledTableCell align="right">Total Pagado</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {lista.map((row) => (
            <StyledTableRow key={row.id}>
              <StyledTableCell component="th" scope="row">
                {row.id}
              </StyledTableCell>
              <StyledTableCell component="th" scope="row">
                {row.nombre}
              </StyledTableCell>
              <StyledTableCell align="right">{row.primerApellido}</StyledTableCell>
              <StyledTableCell align="right">{row.segundoApellido}</StyledTableCell>
              <StyledTableCell align="right">{row.tipoDocumento}</StyledTableCell>
              <StyledTableCell align="right">{row.nroDocumento}</StyledTableCell>
              <StyledTableCell align="right">{row.fechaDeCompra}</StyledTableCell>
              <StyledTableCell align="right">{row.totalPagado} Bs.</StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

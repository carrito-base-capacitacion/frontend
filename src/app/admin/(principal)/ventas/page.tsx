import { Button, Grid, IconButton, Typography } from "@mui/material";
import TablaVentas from "./ui/TablaVentas";
export default function VentasPage() {
    return <>
        <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
        >
            <Typography variant={'h5'} sx={{ fontWeight: 'medium' }}>
                Historial de Ventas
            </Typography>
        </Grid>
        <br />
        <TablaVentas />
    </>
}
'use client'
import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Servicios } from '@/services';
import { Constantes } from '@/config/Constantes';
import { usePathname, useRouter } from 'next/navigation';
import { delay } from '@/utils';
import { useSession } from '@/hooks';

export interface EliminarProductoProps {
    id: string,
    actualizarLista: ()=>void
  }


export default function EliminarProductoDialog({id, actualizarLista}:EliminarProductoProps) {
  const [open, setOpen] = React.useState(false);
  const { sesionPeticion } = useSession()
  const pathname = usePathname();
    const router = useRouter();

  const handleClickOpen = () => {
    setOpen(true);
    console.log(id)
  };

  const handleClose = () => {
    setOpen(false);
  };

  const eliminar = async () => {
    console.log('eliminando')
    // const respuesta = await Servicios.delete({
    //     url: `${Constantes.baseUrl}/productos/${id}`,
    // });
    const respuesta = await sesionPeticion({
      url: `${Constantes.baseUrl}/productos/${id}`,
      tipo: 'delete',
    })
    handleClose();
    await delay(300)
    actualizarLista()
  } 

  return (
    <React.Fragment>
      <Button size="small" color="error" onClick={handleClickOpen}>Eliminar</Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"¿Esta seguro que desea eliminar este producto?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Después de ser eliminado no podra ser recuperado.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancelar</Button>
          <Button onClick={eliminar}>
            Eliminar
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
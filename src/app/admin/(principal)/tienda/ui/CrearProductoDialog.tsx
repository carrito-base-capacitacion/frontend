"use client"
import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Producto } from './producto';
import { Servicios } from '@/services';
import { Constantes } from '@/config/Constantes';
import { usePathname, useRouter } from 'next/navigation';
import { delay } from '@/utils';
import { useAlerts, useSession } from '@/hooks';

export interface CrearProductoDialogProps {
    accionCorrecta: () => void
}

export default function CrearProductoDialog({ accionCorrecta }: CrearProductoDialogProps) {
    const [open, setOpen] = React.useState(false);
    const { sesionPeticion } = useSession()
    const { Alerta } = useAlerts()
    const pathname = usePathname();
    const router = useRouter();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const submitForm = async (producto: Producto) => {
        // const respuesta = await Servicios.post({
        //     url: `${Constantes.baseUrl}/productos`,
        //     body: producto,
        // }).then(async (res) => {
        //     await delay(200)
        //     Alerta({
        //         mensaje: 'Producto creado con exito.',
        //         variant: 'success',
        //       })
        //     accionCorrecta()
        // })
        // .catch((error) => {
        //     console.error(error);
        //     Alerta({
        //         mensaje: 'Error producto no creado.',
        //         variant: 'error',
        //       })
        // });

        const respuesta = await sesionPeticion({
            url: `${Constantes.baseUrl}/productos`,
            tipo: 'post',
            body: producto
        }).then(async (res) => {
            await delay(200)
            Alerta({
                mensaje: 'Producto creado con exito.',
                variant: 'success',
            })
            accionCorrecta()
        })
            .catch((error) => {
                console.error(error);
                Alerta({
                    mensaje: 'Error producto no creado.',
                    variant: 'error',
                })
            });
    };

    return (
        <React.Fragment>
            <Button variant="contained" onClick={handleClickOpen}>Crear Producto</Button>
            <Dialog
                open={open}
                onClose={handleClose}
                PaperProps={{
                    component: 'form',
                    onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
                        event.preventDefault();
                        const formData = new FormData(event.currentTarget);
                        const formJson = Object.fromEntries((formData as any).entries());
                        let producto: Producto = {
                            nombre: formJson.nombre,
                            imagen: formJson.imagen,
                            descripcion: formJson.descripcion,
                            precio: parseInt(formJson.precio),
                        };
                        submitForm(producto);
                        handleClose();
                    },
                }}
            >
                <DialogTitle>Crear Producto</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Llene los campos para crear un nuevo producto para la venta.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        required
                        margin="dense"
                        id="nombre"
                        name="nombre"
                        label="Nombre del Producto"
                        type="text"
                        fullWidth
                        variant="standard"
                    />
                    <TextField
                        autoFocus
                        required
                        margin="dense"
                        id="descripcion"
                        name="descripcion"
                        label="Descripción"
                        type="text"
                        fullWidth
                        variant="standard"
                    />
                    <TextField
                        autoFocus
                        required
                        margin="dense"
                        id="imagen"
                        name="imagen"
                        label="URL Imagen"
                        type="text"
                        fullWidth
                        variant="standard"
                    />
                    <TextField
                        autoFocus
                        required
                        margin="dense"
                        id="precio"
                        name="precio"
                        label="Precio"
                        type="number"
                        fullWidth
                        variant="standard"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancelar</Button>
                    <Button type="submit">Crear</Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

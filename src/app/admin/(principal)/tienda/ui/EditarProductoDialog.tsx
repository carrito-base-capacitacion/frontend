"use client"
import React, { useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Producto } from './producto';
import { Servicios } from '@/services';
import { Constantes } from '@/config/Constantes';
import { usePathname, useRouter } from 'next/navigation';
import { delay } from '@/utils';
import { useSession } from '@/hooks';

export interface EditarProductoProps {
  id: string,
  nombre: string,
  descripcion: string,
  imagen: string,
  precio: number,
  actualizarLista: () => void
}

export default function EditarProductoDialog({ id, nombre, descripcion, imagen, precio, actualizarLista }: EditarProductoProps) {
  const [open, setOpen] = useState(false);
  const { sesionPeticion } = useSession()
  const [formValues, setFormValues] = useState({
    nombre,
    descripcion,
    imagen,
    precio: precio.toString(),
  });

  const pathname = usePathname();
  const router = useRouter();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));
  };

  const submitForm = async (id: string, producto: Producto) => {
    // const respuesta = await Servicios.patch({
    //   url: `${Constantes.baseUrl}/productos/${id}`,
    //   body: producto,
    // }).then(async (res) => {
    //   await delay(300)
    //   actualizarLista()
    // })
    //   .catch((error) => {
    //     console.error(error);
    //   });
    const respuesta = await sesionPeticion({
      url: `${Constantes.baseUrl}/productos/${id}`,
      tipo: 'patch',
      body: producto
    }).then(async (res) => {
      await delay(300)
      actualizarLista()
    })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <React.Fragment>
      <Button size="small" onClick={handleClickOpen}>Editar</Button>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{
          component: 'form',
          onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            const producto: Producto = {
              nombre: formValues.nombre,
              imagen: formValues.imagen,
              descripcion: formValues.descripcion,
              precio: parseInt(formValues.precio),
            };
            submitForm(id, producto);
            handleClose();
          },
        }}
      >
        <DialogTitle>Editar Producto</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            required
            margin="dense"
            id="nombre"
            name="nombre"
            label="Nombre del Producto"
            type="text"
            fullWidth
            variant="standard"
            value={formValues.nombre}
            onChange={handleInputChange}
          />
          <TextField
            autoFocus
            required
            margin="dense"
            id="descripcion"
            name="descripcion"
            label="Descripción"
            type="text"
            fullWidth
            variant="standard"
            value={formValues.descripcion}
            onChange={handleInputChange}
          />
          <TextField
            autoFocus
            required
            margin="dense"
            id="imagen"
            name="imagen"
            label="URL Imagen"
            type="text"
            fullWidth
            variant="standard"
            value={formValues.imagen}
            onChange={handleInputChange}
          />
          <TextField
            autoFocus
            required
            margin="dense"
            id="precio"
            name="precio"
            label="Precio"
            type="number"
            fullWidth
            variant="standard"
            value={formValues.precio}
            onChange={handleInputChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancelar</Button>
          <Button type="submit">Editar</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
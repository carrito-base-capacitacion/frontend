'use client'
import { Button, Card, CardActions, CardContent, CardMedia, Grid, Typography } from "@mui/material"
import EliminarProductoDialog from "./EliminarProductoDialog"
import EditarProductoDialog from "./EditarProductoDialog"
import { useAuth } from '@/context/AuthProvider';
import { CasbinTypes } from '@/types';
import { useEffect, useState } from "react";
import { usePathname } from 'next/navigation'
import AgregarProductoDialog from "./AgregarProductoDialog";

export interface ProductoCardProps {
  id: string,
  nombre: string,
  descripcion: string,
  imagen: string,
  precio: number,
  actualizarLista: () => void,
  actualizarListaCompras: () => void,
  productoAgregado: (idProducto: string) => boolean,
}

export const ProductoCard = ({ nombre, descripcion, imagen, precio, id, actualizarLista, actualizarListaCompras, productoAgregado }: ProductoCardProps) => {
  // router para conocer la ruta actual
  const pathname = usePathname()
  const { permisoUsuario, rolUsuario, usuario } = useAuth()
  // Permisos para acciones
  const [permisos, setPermisos] = useState<CasbinTypes>({
    read: false,
    create: false,
    update: false,
    delete: false,
  })
  /// Método que define permisos por rol desde la sesión
  const definirPermisos = async () => {
    setPermisos(await permisoUsuario(pathname))
  }
  useEffect(() => {
    definirPermisos().finally()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return <Card sx={{ minWidth: 240, maxWidth: 270 }} >
    <CardMedia
      component="img"
      height="194"
      image={imagen}
      alt={nombre}
    />
    <CardContent>
      <Typography gutterBottom variant="h5" component="div">
        {nombre}
      </Typography>
      <Typography variant="body2" color="text.secondary">
        {descripcion}
      </Typography>
      <Typography variant="body1" color="text.primary">
        {precio} Bs.
      </Typography>
    </CardContent>
    <CardActions>
      {
        permisos.update && rolUsuario?.rol == 'ADMINISTRADOR' && (<EditarProductoDialog id={id} nombre={nombre} descripcion={descripcion} imagen={imagen} precio={precio} actualizarLista={actualizarLista} />)
      }
      {
        permisos.delete && (<EliminarProductoDialog id={id} actualizarLista={actualizarLista} />)
      }
      {/* {
          permisos.update && rolUsuario?.rol == 'TECNICO' && (<AgregarProductoDialog idUsuario={usuario?.id || ''} idProducto={id} actualizarListaCompras={actualizarListaCompras}/>)
        }
        {
          permisos.update && rolUsuario?.rol == 'TECNICO' && productoAgregado(id) && (<Typography>Producto agregado</Typography>)
        } */}
      {
        permisos.update && rolUsuario?.rol === 'TECNICO' ? (
          productoAgregado(id) ? (
            <Typography variant="body2" color="text.secondary" padding={1}>Producto agregado</Typography>
          ) : (
            <AgregarProductoDialog
              idUsuario={usuario?.id || ''}
              idProducto={id}
              actualizarListaCompras={actualizarListaCompras}
            />
          )
        ) : null
      }
    </CardActions>
  </Card>
}
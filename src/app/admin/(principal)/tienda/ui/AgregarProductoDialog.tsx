import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Servicios } from '@/services';
import { Constantes } from '@/config/Constantes';
import { CarritoVenta } from './producto';
import { delay } from '@/utils';
import { useSession } from '@/hooks';

export interface AgregarProductoProps {
  idUsuario: string,
  idProducto: string,
  actualizarListaCompras: () => void
}

export default function AgregarProductoDialog({ idUsuario, idProducto, actualizarListaCompras }: AgregarProductoProps) {
  const [open, setOpen] = React.useState(false);
  const { sesionPeticion } = useSession()
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const agregarAlCarrito = async (carritoVenta: CarritoVenta) => {
    // const respuesta = await Servicios.post({
    //   url: `${Constantes.baseUrl}/carrito-de-compra`,
    //   body: carritoVenta,
    // })

    const respuesta = await sesionPeticion({
      url: `${Constantes.baseUrl}/carrito-de-compra`,
      tipo: 'post',
      body: carritoVenta,
    })
  }

  return (
    <React.Fragment>
      <Button variant="outlined" onClick={handleClickOpen}>
        Agregar al Carrito
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{
          component: 'form',
          onSubmit: async (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            const formData = new FormData(event.currentTarget);
            const formJson = Object.fromEntries((formData as any).entries());
            const carritoVenta: CarritoVenta = {
              idUsuario: idUsuario,
              idProducto: idProducto,
              cantidad: parseInt(formJson.cantidad)
            }
            console.log(carritoVenta);
            agregarAlCarrito(carritoVenta);
            handleClose();
            await delay(300)
            actualizarListaCompras();
          },
        }}
      >
        <DialogTitle>Cantidad a agregar</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            required
            margin="dense"
            id="name"
            name="cantidad"
            label="Cantidad del Producto"
            type="number"
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancelar</Button>
          <Button type="submit">Agregar</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}

export interface Producto {
    id?: string,
    nombre: string,
    descripcion: string,
    precio: number,
    imagen: string,
  }

export interface ItemCarritoVenta{
  cantidad: number,
  estado: string,
  id: string,
  producto: Producto,
  venta: Map<string, any>,
}

export interface CarritoVenta {
  idUsuario: string,
  idProducto: string;
  cantidad: number,
}

export enum VentaEnum {
  VENDIDO = 'VENDIDO',
  CANCELADO = 'CANCELADO',
  EN_PROGRESO = 'EN_PROGRESO',
}
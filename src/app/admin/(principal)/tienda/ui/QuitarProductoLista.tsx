'use client'
import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { IconoTooltip } from '@/components/botones/IconoTooltip';
import { delay } from '@/utils';

export interface QuitarProductoListaProps {
    idCarrito: string,
    idProducto: string,
    quitarProducto: (id: string) => void;
}

export default function QuitarProductoLista({idProducto, quitarProducto, idCarrito}:QuitarProductoListaProps) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = async () => {
    quitarProducto(idCarrito)
    setOpen(false);
  };

  return (
    <React.Fragment>
      <IconoTooltip
        id={`editarCompra`}
        titulo={'Eliminar'}
        color={'error'}
        icono={'delete'}
        name={'Eliminar'} accion={handleClickOpen}
         />
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"¿Esta seguro de quitar este producto de la lista?"}
        </DialogTitle>
        <DialogContent>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancelar</Button>
          <Button onClick={handleClose} autoFocus>
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}

'use client'
import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { IconButton, Table, TableBody, TableCell, TableContainer, TableFooter, TableHead, TableRow } from '@mui/material';

import { useAuth } from '@/context/AuthProvider';
import { CasbinTypes } from '@/types';
import { useEffect, useState } from "react";
import { usePathname, useRouter } from 'next/navigation';

import Badge, { BadgeProps } from '@mui/material/Badge';
import { styled } from '@mui/material/styles';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { Servicios } from "@/services";
import { Constantes } from "@/config/Constantes";
import Paper from '@mui/material/Paper';
import { IconoTooltip } from '@/components/botones/IconoTooltip';
import QuitarProductoLista from './QuitarProductoLista';
import { Alerta } from 'stories/components/organismos/dialogos/AlertDialog.stories';
import { useAlerts, useSession } from '@/hooks';
import { delay } from '@/utils';
import { VentaEnum } from './producto';

const StyledBadge = styled(Badge)<BadgeProps>(({ theme }) => ({
  '& .MuiBadge-badge': {
    right: -3,
    top: 13,
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
  },
}));

export interface ListaDeComprasDialogProps {
  lista: any[],
  quitarProductoLista: (id: string) => void,
  operarCantidad: (id: string, sumar: boolean, cantidad: number) => void,
  vaciarLista: () => void
}

export default function ListaDeComprasDialog({ lista, quitarProductoLista, vaciarLista, operarCantidad }: ListaDeComprasDialogProps) {
  const [open, setOpen] = React.useState(false);
  const { sesionPeticion } = useSession()
  const router = useRouter();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  //  const [lista, setLista ] = useState<any[]>([]);

  // router para conocer la ruta actual
  const pathname = usePathname()
  const { permisoUsuario, rolUsuario, usuario } = useAuth()
  // Permisos para acciones
  const [permisos, setPermisos] = useState<CasbinTypes>({
    read: false,
    create: false,
    update: false,
    delete: false,
  })
  /// Método que define permisos por rol desde la sesión
  const definirPermisos = async () => {
    setPermisos(await permisoUsuario(pathname))
  }

  // const recuperarDatos = async () => {
  //   const respuesta = await Servicios.peticion({
  //     url: `${Constantes.baseUrl}/carrito-de-compra/${usuario?.id}`,
  //     tipo: 'get',
  //   })
  //   console.log(respuesta, 'UUUUUUUUUUUUUUUUU')
  //   setLista(respuesta);
  //   return respuesta;
  // }

  const calcularTotal = () => {
    let res = 0;
    lista.forEach((item) => {
      res += parseInt(item['producto']['precio']) * parseInt(item['cantidad']);
    })
    return res;
  }

  const quitarProducto = async (id: string) => {
    // setLista(lista.filter((item)=> item['id'] != id));
    quitarProductoLista(id);
    // const respuesta = await Servicios.delete({
    //   url: `${Constantes.baseUrl}/carrito-de-compra/${id}`
    // })
    const respuesta = await sesionPeticion({
      url: `${Constantes.baseUrl}/carrito-de-compra/${id}`,
      tipo: 'delete',
    })
    await delay(200)
  }

  const operarCantidadProducto = async (id: string, sumar: boolean, cantidad: number) => {
    operarCantidad(id,sumar,cantidad)
    const respuesta = sumar ? await sesionPeticion({
      url: `${Constantes.baseUrl}/carrito-de-compra/${id}/sumar`,
      tipo: 'patch',
    }) : cantidad -1 != 0?await sesionPeticion({
      url: `${Constantes.baseUrl}/carrito-de-compra/${id}/restar`,
      tipo: 'patch',
    }): null;
    await delay(200)
  }

  const { Alerta } = useAlerts()

  const iniciarLista = async () => {
    await delay(100)
    // setLista(listaCompra)
    // console.log(listaCompra, 'Lista de compras')
  }

  useEffect(() => {
    definirPermisos().finally()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    // setLista(listaCompra)
    console.log('se actulizo: ', lista)
  }, [lista])

  return (
    <React.Fragment>
      {
        permisos.update && rolUsuario?.rol == 'TECNICO' && (<IconButton onClick={handleClickOpen} aria-label="cart">
          <StyledBadge badgeContent={lista.length} color="error">
            <ShoppingCartIcon />
          </StyledBadge>
        </IconButton>)
      }
      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{
          component: 'form',
          onSubmit: async (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            const formData = new FormData(event.currentTarget);
            const formJson = Object.fromEntries((formData as any).entries());
            if (lista.length > 0) {
              const idVenta = lista[0]['venta']['id'];
              await delay(1000)
              // const respuesta = await Servicios.peticion({
              //     url: `${Constantes.baseUrl}/carrito-de-compra/${idVenta}/cambiar-estado?estado=${VentaEnum.VENDIDO}`,
              //     tipo: 'post',
              //   }).then((e)=>{
              //     vaciarLista()
              //     Alerta({
              //       mensaje: 'Compra Exitosa',
              //       variant: 'success',
              //     })
              //     router.push(`${pathname}`);
              //   }).catch((e)=>{
              //     Alerta({ mensaje: 'Error al realizar la compra', variant: 'error' })
              //   })

              const respuesta = await sesionPeticion({
                url: `${Constantes.baseUrl}/carrito-de-compra/${idVenta}/cambiar-estado?estado=${VentaEnum.VENDIDO}`,
                tipo: 'post',
              }).then((e) => {
                vaciarLista()
                Alerta({
                  mensaje: 'Compra Exitosa',
                  variant: 'success',
                })
                router.push(`${pathname}`);
              }).catch((e) => {
                Alerta({ mensaje: 'Error al realizar la compra', variant: 'error' })
              })
            }
            handleClose();
          },
        }}
      >
        <DialogTitle>Carrito de Compras</DialogTitle>
        <DialogContent>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 450 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Nombre del producto</TableCell>
                  <TableCell align="right">Cantidad</TableCell>
                  <TableCell align="right">Precio</TableCell>
                  <TableCell align="center">Acciones</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {lista.map((row) => (
                  <TableRow
                    key={row['id']}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row['producto']['nombre']}
                    </TableCell>
                    <TableCell align="right">{row['cantidad']}</TableCell>
                    <TableCell align="right">{row['producto']['precio']}</TableCell>
                    <TableCell align="right">
                      <IconoTooltip
                        id={`agregarCantidad`}
                        titulo={'Agregar Cantidad'}
                        color={'primary'}
                        icono={'add'}
                        name={'Agregar'} accion={() => operarCantidadProducto(row['id'], true, row['cantidad'])}
                      />
                      <IconoTooltip
                        id={`bajarCantidad`}
                        titulo={'Restar Cantidad'}
                        color={'primary'}
                        icono={'remove'}
                        name={'Restar'} accion={() => operarCantidadProducto(row['id'], false, row['cantidad'])}
                      />
                      <QuitarProductoLista idProducto={row['producto']['id']} quitarProducto={quitarProducto} idCarrito={row['id']} />
                    </TableCell>
                  </TableRow>
                ))}
                <TableFooter >
                  <TableCell style={{ fontSize: '1rem' }} >Total: </TableCell>
                  <TableCell style={{ fontSize: '1rem' }} >{calcularTotal()} Bs.</TableCell>
                </TableFooter>
              </TableBody>
            </Table>
          </TableContainer>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancelar</Button>
          <Button variant="contained" type="submit" disabled={lista.length == 0}>Comprar</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}

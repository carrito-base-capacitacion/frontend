import { Constantes } from "@/config/Constantes"
import { Servicios } from "@/services"
import { ItemCarritoVenta, Producto } from "./producto"
import { Grid } from "@mui/material"
import { ProductoCard } from "./ProductoCard"

export interface ListaDeProductosProps {
    lista: Producto[],
    actualizarLista: () => void,
    actualizarListaCompras: () => void,
    listaCompras: ItemCarritoVenta[],
}

export const ListaDeProductos = async ({ lista, actualizarLista, actualizarListaCompras, listaCompras }: ListaDeProductosProps) => {
    // const respuesta = await Servicios.peticion({
    //     url: `${Constantes.baseUrl}/productos`,
    //     tipo: 'get',
    // })

    const productoAgregado = (idProducto: string) => {
        const res = listaCompras.find(item => item.producto['id'] === idProducto);
        if (res && Object.keys(res).length > 0) {
            return true;
        } else {
            return false;
        }
    }
    return <>
        <Grid container spacing={{ xs: 3, md: 3 }} columns={{ xs: 4, sm: 7, md: 12 }}>
            {lista.map((producto: Producto) => <Grid item>
                <ProductoCard nombre={producto.nombre} descripcion={producto.descripcion} imagen={producto.imagen} precio={producto.precio} id={producto.id ? producto.id : ''} actualizarLista={actualizarLista} actualizarListaCompras={actualizarListaCompras} productoAgregado={productoAgregado}/>
            </Grid>)
            }
        </Grid>
    </>
}
'use client'
import { Button, Grid, IconButton, Typography } from "@mui/material";
import { ListaDeProductos } from "./ui/ListaDeProductos";
import CrearProductoDialog from "./ui/CrearProductoDialog";
import { useAuth } from '@/context/AuthProvider';
import { CasbinTypes } from '@/types';
import { useEffect, useState } from "react";
import { usePathname } from 'next/navigation';
import ListaDeComprasDialog from "./ui/ListaDeComprasDialog";
import { Servicios } from "@/services";
import { Constantes } from "@/config/Constantes";
import { delay } from "@/utils";
import { useSession } from "@/hooks";
import { ItemCarritoVenta } from "./ui/producto";

export default function TiendaPage() {
    // router para conocer la ruta actual
    const pathname = usePathname()
    const { permisoUsuario, rolUsuario, usuario } = useAuth()
    const { sesionPeticion } = useSession()
    const [lista, setLista] = useState([]);
    const [listaCompras, setListaCompras] = useState<ItemCarritoVenta[]>([]);
    const recuperarDatos = async () => {
        // const respuesta = await Servicios.peticion({
        //     url: `${Constantes.baseUrl}/productos`,
        //     tipo: 'get',
        // })

        const respuesta = await sesionPeticion({
            url: `${Constantes.baseUrl}/productos`,
            tipo: 'get',
        })
        setLista(respuesta);
    }
    const recuperarDatosCompras = async () => {
        // const respuesta = await Servicios.peticion({
        //     url: `${Constantes.baseUrl}/carrito-de-compra/${usuario?.id}`,
        //     tipo: 'get',
        // })

        const respuesta = await sesionPeticion({
            url: `${Constantes.baseUrl}/carrito-de-compra/${usuario?.id}`,
            tipo: 'get',
        })
        setListaCompras(respuesta);
        return respuesta;
    }

    const quitarProductoCompras = (id: string) => {
        setListaCompras(listaCompras.filter((item) => item['id'] != id));
    }

    const vaciarListaCompras = () => {
        setListaCompras([]);
    }

    const operarCantidad = (idLista: string, sumar: boolean, cantidad: number) => {
        // listaCompras.forEach((row) => {
        //     if (row.id == idLista) {
        //         if ('cantidad' in row) {
        //             row.cantidad = sumar ? row.cantidad + 1 : row.cantidad - 1;
        //         }
        //         console.log(row, ' producto a operar')
        //     }
        // })

        const aux = listaCompras.map(row => {
            if(row.id == idLista) {
                return {...row, cantidad: sumar? row.cantidad + 1 : cantidad-1!=0?row.cantidad - 1:1}
            }
            return row;
        })
        setListaCompras(aux)
    }

    // Permisos para acciones
    const [permisos, setPermisos] = useState<CasbinTypes>({
        read: false,
        create: false,
        update: false,
        delete: false,
    })
    /// Método que define permisos por rol desde la sesión
    const definirPermisos = async () => {
        setPermisos(await permisoUsuario(pathname))
    }
    useEffect(() => {
        definirPermisos().finally()
        // eslint-disable-next-line react-hooks/exhaustive-deps
        recuperarDatosCompras().finally()
        recuperarDatos().finally()
    }, [])
    return <>
        <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
        >
            <Typography variant={'h5'} sx={{ fontWeight: 'medium' }}>
                Lista de Productos
            </Typography>
            {
                permisos.create && (<CrearProductoDialog accionCorrecta={() => { recuperarDatos().finally() }} />)
            }
            <ListaDeComprasDialog lista={listaCompras} quitarProductoLista={quitarProductoCompras} vaciarLista={vaciarListaCompras} operarCantidad={operarCantidad} />
        </Grid>
        <br />
        <ListaDeProductos lista={lista} actualizarLista={() => { recuperarDatos().finally() }} actualizarListaCompras={() => { recuperarDatosCompras().finally() }} listaCompras={listaCompras}/>
    </>
}
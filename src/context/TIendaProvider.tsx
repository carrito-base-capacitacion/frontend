'use client'

import { Producto, ProductoCantidad } from "@/app/admin/(principal)/tienda/ui/producto";
import { useState, createContext, ReactNode, Dispatch, SetStateAction, useContext } from "react";

interface TiendaProviderProps {
  children: ReactNode;
}

interface TiendaContextProps {
  cantidad: number;
  setCantidad: Dispatch<SetStateAction<number>>;
  productos: ProductoCantidad[];
  setProductos: Dispatch<SetStateAction<ProductoCantidad[]>>;
}

const TiendaContext = createContext<TiendaContextProps | null>(null);

export function TiendaProvider({ children }: TiendaProviderProps) {
  const [cantidad, setCantidad] = useState<number>(0);
  const [productos, setProductos] = useState<ProductoCantidad[]>([]);

  const contextValue: TiendaContextProps = {
    cantidad,
    setCantidad,
    productos,
    setProductos
  };

  const eliminarProducto = (id: string) => {
    setCantidad(cantidad-1);
    const nuevaListaProductos = productos.filter(producto => producto.producto.id !== id);
    setProductos(nuevaListaProductos);
  }

  return (
    <TiendaContext.Provider value={contextValue}>
      {children}
    </TiendaContext.Provider>
  );
}

export const useTienda = () => useContext(TiendaContext)